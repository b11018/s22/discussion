// alert('Hello from the other side')

// Array Methods

// Mutator Methods

let fruits = ["Apple", "Orange", "Kiwi", "Dragon", "Fruit"];
console.log(fruits);

// push()
/*
	Syntax:
		arrayName.push("value");
*/
let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
// result: 5
console.log("Mutated through push: " + fruits);
console.log(fruits);

// add multiple elements in an array
fruits.push("Avocado", "Guava");
console.log("Mutated with multiple elements: " + fruits);
console.log(fruits);

// pop();
/*
	Syntax:
		arrayName.pop();
*/
let removedFruit = fruits.pop();
console.log(removedFruit);
// result: Guava removed
console.log("Mutated through pop: " + fruits);
console.log(fruits);

// unshift();
/*
	Syntax: arrayName.unshift("value");
			arrayName.unshift("valueA", "valueB");
*/

fruits.unshift("Lime", "Banana");
console.log(fruits);

// shift();
/*
	Syntax:
		arrayName.shift();
*/
let anotherFruit = fruits.shift();
console.log(anotherFruit);
// result: Lime removed
console.log("Mutated through shift: " + fruits);
console.log(fruits);

function removeFirst(){
	fruits.shift()
	console.log(fruits);
}

removeFirst();

// splice();
/*
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
*/

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated through splice: " + fruits);
console.log(fruits);

// another example:
fruits.splice(3, 1, "Orange");
console.log("Mutated through splice again: " + fruits);
console.log(fruits);

// another example:
fruits.splice(0, 2);
console.log("Mutated through splice without adding: " + fruits)

// sort();
/*
	Syntax:
		arrayName.sort();
*/
fruits.sort();
console.log("Mutated through sort: " + fruits);
console.log(fruits);

// reverse();
/*
	Syntax:
		arrayName.reverse();
*/
fruits.reverse();
console.log("Mutated through reverse: " + fruits);
console.log(fruits);

// Non-Mutator Methods
console.log("Non-Mutator Methods starts here:")
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
let countries2 = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE', 'PH'];

// indexOf();
/*
	Syntax:
		arrayName.indexOf(searchValue);
*/

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf: " + firstIndex);
// result: 1

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf again: " + invalidCountry);
// result: -1

// lastIndexOf();
/*
	Syntax:
		arrayName.lastIndexOf(searchValue)
		arrayName.lastIndexOf(searchValue, limitIndex);
*/

let lastIndex = countries.lastIndexOf("PH");
console.log('Result of lastIndexOf ' + lastIndex);
// result: 5

let lastIndexStart = countries2.lastIndexOf("PH", 6)
console.log('Result of lastIndexOf again ' + lastIndexStart);
// result: 5
// console.log(countries);

// slice();
/*
	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

console.log(countries)
let slicedArrayA = countries.slice(2);
console.log('Result from slice with startingIndex only: ' + slicedArrayA);
// result:
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log('Result from slice with startingIndex and endingIndex: ' + slicedArrayB);
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log('Result from slice with negative value: ' + slicedArrayC)
console.log(slicedArrayC);

console.log(countries);


//  toString()
/*
	Syntax:
		arrayName.toString();
*/
let stringArray = countries.toString();
console.log('Result from toString: ');
console.log(stringArray);

// concat()
/*
	Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
*/

let tasksArrayA = ['drink html', 'eat javascript'];
let tasksArrayB = ['inhale css', 'breathe bootstrap'];
let tasksArrayC = ['get git', 'be node'];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log('Result from concat method: ')
console.log(tasks)

// combining multiple arrays
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log('Result from concat with all arrays: ');
console.log(allTasks);

// combining arrays with elements
let combinedTasks = tasksArrayA.concat('smell express', 'throw react');
console.log('Result from concat with elements:');
console.log(combinedTasks);

// join();
/* 
	Syntax:
		arrayName.join('separator string')
*/
let users = ["John", "Robert", "Jane", "Joe"];

console.log(users.join());
// separator: John,Robert,Jane,Joe
console.log(users.join(''));
// separator: JohnRobertJaneJoe
console.log(users.join(' - '));
// result: John - Robert - Jane - Joe

// includes();
/*
	Syntax:
		arrayName.includes(<argumentToFind>)
*/

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes('Mouse');
console.log(productFound1);
// result: true

let productFound2 = products.includes("Headset");
console.log(productFound2);
// result: false

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
})
console.log(filteredProducts);

// Iteration Methods

// forEach();
/* 
	Syntax:
		arrayName.forEach(function(indivElement){
			statement/code block
		});
*/

allTasks.forEach(function(task){
	console.log(task);
});

// use forEach with conditional statements
let filteredTasks = [];

allTasks.forEach(function(task){
	// console.log(task)

	if(task.length > 10){

		console.log(task);
		filteredTasks.push(task)
	}
});
console.log('Results of filteredTasks: ');
console.log(filteredTasks);

// map();
/* 
	Syntax:
		let / const resultArray = arrayName.map(function(indivElement){
			return statement/code block
		})
*/

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){

	return number * number;
});

console.log('Original array:')
console.log(numbers); // original is unaffected by map()
console.log('Result of map method:')
console.log(numberMap); // a new array returned by map() and stored in a variable

// every();
/* 
	Syntax:
		let / const resultArray = arrayName.every(function(indivElement){
			return code block / statement (condition)
		});
*/

let allValid = numbers.every(function(number){
	return (number < 3)
});

console.log('Result from every method:')
console.log(allValid);
// result: false

// some();
/* 
	Syntax:
		let / const resultArray = arrayName.some(function(){
			return statement / code block (condition)
		})
*/
let someValid = numbers.some(function(number){
	return (number < 2);
});

console.log('Result from some method:')
console.log(someValid);
// result: true

if(someValid){
	console.log('Some numbers in the array are less than 2.')
};

// filter()
/* 
	Syntax:
		let / const resultArray = number.filter(function(indivElement){
			return statement code block (condition)
		});
*/
let filterValid = numbers.filter(function(number){
	return (number < 3);
});
console.log('Result of filter method:');
console.log(filterValid);

let nothingFound = numbers.filter(function(number){
	return (number > 5);
});
console.log('Result of filter method with nothing found: ')
console.log(nothingFound);

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
});
console.log('Filtering through forEach:')
console.log(filteredNumbers);

// reduce();
/*
	Syntax:
		let / const resultArray = arrayName.reduce(function(accumulator, currentValue){
			return expression / operation
		})
		// accumulator - holds the result of the operation
*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn('current iteration: ' + ++iteration)
	console.log('accumulator: ' + x);
	console.log('currentValue: ' + y);

	// operation
	return x + y
});
console.log('Result from reducedArray: ' + reducedArray);

